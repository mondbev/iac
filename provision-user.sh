#!/usr/bin/env bash 
#########################################
#created by: Loud-Mobius
#purpose to install and setup ansible env
#date: 08/03/2021
#version: v1.4.19
########################################

install_pipenv(){
if [[ -d "$HOME/iac" ]]; then
	exit 0
fi
mkdir iac && cd iac
pip3 install pipenv --user
pipenv shell
pip3 install ansible
echo -n "
[localhost]
127.0.0.1
[ans]
172.28.128.101
[web]
172.28.128.102
" > hosts
echo -n "
[defaults]
inventory = ./hosts
" > ansible.cfg
echo
echo "done"
echo
}
get_ssh(){
echo $(hostname) 
ssh-keygen -b 2048 -t rsa -f /home/vagrant/.ssh/id_rsa2 -q -N ""
cat /home/vagrant/.ssh/id_rsa2.pub >> /home/vagrant/.ssh/authorized_keys
cat /home/vagrant/.ssh/id_rsa2.pub
echo $(hostname)
sshd_conf="/etc/ssh/sshd_config"
sudo sed -i 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' $sshd_conf
sudo sed -i 's/GSSAPIAuthentication/#GSSAPIAuthentication/g' $sshd_conf
sudo sed -i 's/GSSAPICleanupCredentials/#GSSAPICleanupCredentials/g' $sshd_conf
sudo sed -i 's/PubkeyAuthentication no/PubkeyAuthentication yes/g' $sshd_conf
echo "sed done"
sudo systemctl daemon-reload
sudo systemctl restart sshd
#pipenv shell
#ansible all -u vagrant -m ping
}
###
#Main
###
install_pipenv
get_ssh
