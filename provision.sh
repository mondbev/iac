#!/usr/bin/env bash 
#########################################
#created by: Silent-Mobius
#purpose to install and setup ansible env
#date: 08/03/2021
#version: v1.4.19
########################################
#Variables
_installer=${1:-'yum'}
_pkgs=(epel-release python3 python3-pip sshpass)

#Functions
install_pkgs(){

	for _pkg in ${_pkgs[@]}
		do
			$_installer install -y $_pkg
		done

}

###
#Main
###
if [[ $EUID != 0 ]];then
	echo $msg_root
	exit 1
else

	install_pkgs
fi
### TODO ###
# web machine:
# - echo ans ssh key to key file
# ans machine:
# - yum install ansible -y
# - mkdir iac && cd iac
# - pip3 install pipenv --user
# - pipenv shell
# - pip3 install ansible
# - hosts:
# 	- [web_server] \n 192.168.112.111
# 	- [db_server] \n  192.168.112.110
# - ansible.cfg:
#	- [defaults] \n inventory = ./hosts
# - ansible web_server -u vagrant  -m ping
